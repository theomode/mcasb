from django import template
from django.utils.html import mark_safe

from markdown2 import markdown

register = template.Library()

@register.filter(name="markdown_to_html")
def markdown_to_html(markdown_text):
	return mark_safe(markdown(markdown_text))
