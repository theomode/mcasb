from django.db import models
from s3direct.fields import S3DirectField

# Create your models here.

class Upload(models.Model):
	name = models.CharField(max_length=255, unique=True, blank=False, help_text='Make it specific but avoid explicit language.')
	upload	= S3DirectField(dest='imgs', blank=True)

	def __unicode__(self):
		return "%s" % self.name

	def url(self):
		return "%s%s" % (MEDIA_URL, self.upload) if self.upload else None

	def file_extention(self):	
		return str(self.upload).split(".")[-1]

	def is_image(self):
		return self.file_extention() in ['jpg', 'jpeg', 'png', 'gif', 'bmp']

