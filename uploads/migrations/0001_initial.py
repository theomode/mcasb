# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('upload', s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
