from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from .sitemaps import sitemaps as sitemaps

from mcasb import views
from pages.views import detail as page_detail
from articles.views import display_article

from blog.feeds import LatestPosts


urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('splash.urls'),  name='home'),
    url(r'^gallery/', include('gallery.urls'),  name='gallery'),
    url(r'^blog/', include('blog.urls')),
    url(r'^rss/', LatestPosts()),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^s3direct/', include('s3direct.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^studio/library/(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_article),
    url(r'^(?P<page_url>[A-Za-z0-9\/_\-\.]+)/$', page_detail),
    
)
