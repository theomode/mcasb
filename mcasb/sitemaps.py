from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import Site
from django.contrib.sitemaps import GenericSitemap
from django.core.urlresolvers import reverse
from django.contrib import sitemaps
from django.core.urlresolvers import reverse

from alumni.models import Alumni
from articles.models import Article
from blog.models import Post
from classes.models import Class 
from faculty.models import Faculty
from gallery.models import Video, SlidePhoto, Friend
from pages.models import Page
from programs.models import Program



class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['', 'gallery', 'alumni']

    def location(self, item):
        return '/' + item



articles_dict = {
	'queryset': Article.objects.all()
}
blog_dict = {
	'queryset': Post.objects.all()
}
pages_dict = {
	'queryset': Page.objects.all()
}

sitemaps = {
    'static': StaticViewSitemap(),
    'articles': GenericSitemap(articles_dict, priority=0.6),
    'blog': GenericSitemap(blog_dict, priority=0.6),
	'pages' : GenericSitemap(pages_dict, priority=0.6),			
}