"""
Django settings for mcasb project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8c7r79o2xbjar0oq6o2vnf#l#k@ugmx4a*m3@g#yb9z3=l2dsl'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DEFAULT_CHARSET = 'utf-8'
DEFAULT_CONTENT_TYPE = 'text/html'

ALLOWED_HOSTS = []


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.getcwd()+'/mcasb/templates',
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                "django.core.context_processors.i18n",
                "django.core.context_processors.media",
                "mcasb.context_processors.initial", 

               
            ],

            'debug': DEBUG,
  
            # 'loaders': [
            #     ('django.template.loaders.cached.Loader', [
            #         'django.template.loaders.filesystem.Loader',
            #         'django.template.loaders.app_directories.Loader',
            #     ]),
            
            # ],
        },
    },
]


SITE_ID = 1
# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',

    
    'blog.apps.BlogConfig',
    'pages.apps.PagesConfig',
    'splash.apps.SplashConfig',
    'gallery.apps.GalleryConfig',
    'alumni.apps.AlumniConfig',
    'faculty.apps.FacultyConfig',
    'programs.apps.ProgramsConfig',
    'classes.apps.ClassesConfig',
    'universal.apps.UniversalConfig',
    'articles.apps.ArticlesConfig',
    'uploads.apps.UploadsConfig',


    's3direct',
    'ckeditor',
    'el_pagination',
    
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'mcasb.urls'


WSGI_APPLICATION = 'mcasb.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DPATH = os.path.dirname(os.path.realpath(__file__))


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
# MEDIA_URL = '/assets/i/'
# MEDIA_ROOT = DPATH + '/assets/i'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_JQUERY_URL = 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        "removePlugins": "stylesheetparser",
        'font_names' : 'Open Sans Condensed, sans-serif;' + 
        'Oswald, sans-serif;' + 
        'Arial/Arial, Helvetica, sans-serif;' +
        'Courier New/Courier New, Courier, monospace;' +
        'Georgia/Georgia, serif;' +
        'Times New Roman/Times New Roman, Times, serif;' +
        'Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;' +
        'Verdana/Verdana, Geneva, sans-serif;'
    },
    
}

AWS_QUERYSTRING_AUTH = False

STATIC_URL = '/static/'
STATIC_ROOT = '/Users/theomode/dev/MCASB/local-mcasb/assets/static/'

STATICFILES_DIRS = (
    'assets/',
)



# AWS keys
AWS_SECRET_ACCESS_KEY = 'BhVM2OsLfi6qXpGHba2yn1mA2CdOXJE6h9wl/Rew'
AWS_ACCESS_KEY_ID = 'AKIAIOD7TCLAA6ICCEEA'
AWS_STORAGE_BUCKET_NAME = 'mcasb'
S3DIRECT_REGION = 'us-west-2'
S3_URL = 'http://%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

def create_filename(filename):
    import uuid
    ext = filename.split('.')[-1]
    filename = '%s.%s' % (uuid.uuid4().hex, ext)
    return os.path.join('images', filename)

S3DIRECT_DESTINATIONS = {
    # # Allow anybody to upload any MIME type
    'misc': ('uploads/misc',),

    # # Allow staff users to upload any MIME type
    'files': ('uploads/files', lambda u: u.is_staff,),

    # Allow anybody to upload jpeg's and png's.
    'imgs': ('uploads/imgs', lambda u: True, ['image/jpeg', 'image/png'],),

    # # Allow authenticated users to upload mp4's
    'vids': ('uploads/vids', lambda u: u.is_authenticated(), ['video/mp4'],),

    # Specify a non-default bucket for PDFs
    'pdfs': ('/', lambda u: True, ['application/pdf'], None, 'pdf-bucket',),

    # # Allow logged in users to upload any type of file and give it a private acl:
    # 'private': (
    #     'uploads/vids',
    #     lambda u: u.is_authenticated(),
    #     '*',
    #     'private')
}

