from universal.models import Footer, HeaderLogo
from alumni.models import Alumni


def initial(request):

    header_logo = HeaderLogo.objects.all()
    footer_text = Footer.objects.all()
    alumni = Alumni.objects.all()

    return { "header_logo" : header_logo, "footer_text" : footer_text, "alumni": alumni }
    