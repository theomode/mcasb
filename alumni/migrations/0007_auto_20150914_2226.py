# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alumni', '0006_alumni_title'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='alumni',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='alumni',
            name='position',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
