# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alumni', '0002_auto_20150624_0154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumni',
            name='image',
            field=models.ImageField(default=b'', upload_to=b'assets/i'),
            preserve_default=True,
        ),
    ]
