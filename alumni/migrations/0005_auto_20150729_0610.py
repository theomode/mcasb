# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('alumni', '0004_auto_20150716_2319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumni',
            name='image',
            field=s3direct.fields.S3DirectField(blank=True),
            preserve_default=True,
        ),
    ]
