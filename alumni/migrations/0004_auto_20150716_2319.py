# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alumni', '0003_auto_20150624_0155'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumni',
            name='accolades',
            field=models.TextField(default=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alumni',
            name='testimonial',
            field=models.TextField(default=b'', blank=True),
            preserve_default=True,
        ),
    ]
