from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField


# Create your models here.
class Alumni(models.Model):
	name 	= models.CharField(max_length=255, default='')
	title 	= models.CharField(max_length=255, blank=True, default='')
	image		= S3DirectField(dest='imgs', blank=True)
	testimonial = models.TextField(blank=True, default='')
	accolades = models.TextField(blank=True, default='')
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['position']

	