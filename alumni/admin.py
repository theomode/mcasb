from django.contrib import admin
from django import forms
from django.db import models

from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


from alumni.models import  Alumni


admin.site.register(Alumni)