from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField

# Create your models here.

class Program(models.Model):
	image		= S3DirectField(dest='imgs', blank=True)
	title 		= models.CharField(max_length=255,blank=True, null=True)
	body		= RichTextField()
	more		= models.BooleanField()	
	more_text	= RichTextField()
	image_left 	= models.BooleanField()	
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['position']
