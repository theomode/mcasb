# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0002_auto_20150729_0409'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='program',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='program',
            name='position',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
