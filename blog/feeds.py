from django.contrib.syndication.views import Feed
from blog.models import Post

class LatestPosts(Feed):
    title = "Michael Chekhov Actor's Studio Boston Blog"
    link = "/blog/"
    description = "Scott Fielding's insight on everything everything"

    def items(self):
        return Post.objects.all()

    def item_title(self, item):
        return item.title

    def item_body(self, item):
        return item.body
