from django.contrib import admin
from blog.models import Post, Billboard
# Register your models here.

class PostAdmin(admin.ModelAdmin):
	ordering = ('position',)
	list_display = ('title', 'position')

class BillboardAdmin(admin.ModelAdmin):
	ordering = ('position',)
	list_display = ('name', 'position')


admin.site.register(Post,PostAdmin)
admin.site.register(Billboard,BillboardAdmin)
