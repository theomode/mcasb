from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField

# Create your models here.

class Post(models.Model):
	main_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	title 		= models.CharField(max_length=255,blank=True, null=True)
	page_url 	= models.SlugField(blank=True, null=True) ##sluggyy
	date 		= models.DateField(blank=True, null=True)	
	body		= RichTextField()
	position 	= models.IntegerField(default=0)


	def __unicode__(self):
		return self.title

	def title_url(self):
		return self.title.replace(" ","-")
	
	def get_absolute_url(self):
		return  '/blog/' + self.page_url
	
	# def save(self, *args, **kwargs):
	# 	if not self.summary:
	# 		self.summary = self.body[:300]
	# 	super(Post, self).save(*args, **kwargs)
	
	class Meta:
		ordering = ['position']

class Billboard(models.Model):
	name 	= models.CharField(max_length=255,blank=True, null=True)
	image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	text	= RichTextField(blank=True)
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['position']


