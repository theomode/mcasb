# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20150811_1554'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='featuredbillboard',
            name='featured',
        ),
        migrations.DeleteModel(
            name='FeaturedBillboard',
        ),
        migrations.AlterModelOptions(
            name='billboard',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='billboard',
            name='position',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
