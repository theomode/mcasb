# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20150729_0108'),
    ]

    operations = [
        migrations.CreateModel(
            name='Billboard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', blank=True)),
                ('text', ckeditor.fields.RichTextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeaturedBillboard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('featured', models.ForeignKey(to='blog.Billboard')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
