from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from models import Post, Billboard
import time

def blog(request):
	post = Post.objects.all()
	billboard = Billboard.objects.all()

	context = { "post" : post, "billboard" : billboard }

	return render(request, 'blog/blog.html', context)


def display_post(request, slug):
	try:
		post = Post.objects.get(page_url=slug)
	except Post.DoesNotExist:
		raise Http404


	link = "/blog/" + post.page_url
	billboard = Billboard.objects.all()

	context = {'post': post, 'link':link, 'billboard': billboard }

	return render(request, 'blog/blog_post.html', context)
