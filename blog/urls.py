from django.conf.urls import patterns, url
from blog import views 
from views import display_post


urlpatterns = patterns('',
	url(r'^$', views.blog),
	url(r'^(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_post, name='blog-post-detail'),
	
)

