from django.contrib import admin
from models import Class

# Register your models here.

class ClassAdmin(admin.ModelAdmin):
	ordering = ('position',)
	list_display = ('title', 'position')


admin.site.register(Class, ClassAdmin)