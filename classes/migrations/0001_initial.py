# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', s3direct.fields.S3DirectField(blank=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('body', ckeditor.fields.RichTextField()),
                ('image_left', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
