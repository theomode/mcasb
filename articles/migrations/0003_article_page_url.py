# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_article_short_desc'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='page_url',
            field=models.SlugField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
