from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField


# Create your models here.
class Article(models.Model):
	title 		= models.CharField(max_length=255, default='')
	author		= models.CharField(max_length=255, default='')
	page_url 	= models.SlugField(blank=True, null=True) ##sluggyy
	date 		= models.DateField(blank=True, null=True)	
	short_desc  = RichTextField()
	body 		= RichTextField()
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title


	def get_absolute_url(self):
		return  '' + self.page_url

	class Meta:
		ordering = ['position']
