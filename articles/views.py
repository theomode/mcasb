from django.shortcuts import render

from articles.models import Article
# Create your views here.
def display_article(request, slug):
	try:
		article = Article.objects.get(page_url=slug)
	except Article.DoesNotExist:
		raise Http404


	link = "/library/" + article.page_url

	context = {'article': article, 'link':link}
	

	return render(request, 'pages/studio/_article.html', context)
