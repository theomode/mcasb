from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404


import context_loaders as loaders
from models import Page
from faculty.models import Faculty
from classes.models import Class
from programs.models import Program
from alumni.models import Alumni
from articles.models import Article


'''
	Used only if pages require aditional context
'''

def get_template_context(template_url, request):
	template_map = {
		'pages/splash.html': loaders.DefaultPageContextLoader,
		'pages/generic.html': loaders.DefaultPageContextLoader,
		'blog/blog.html': loaders.DefaultPageContextLoader,
		'people/people.html': loaders.DefaultPageContextLoader,
		'events/events.html': loaders.DefaultPageContextLoader,
		'pages/portfolio.html': loaders.DefaultPageContextLoader,
	}
	context_loader = template_map.get(template_url, loaders.DefaultPageContextLoader)
	return context_loader(request).get_context()

def detail(request, page_url=''):
	root = ''
	try:
		page = Page.objects.get(url=page_url)
	except Page.DoesNotExist:
		raise Http404


	content = Page.objects.all()
	faculty = Faculty.objects.all()
	classes = Class.objects.all()
	programs = Program.objects.all()
	alumni = Alumni.objects.all()
	article = Article.objects.all()
	

	# for snippet in page.snippet_set.all():
	# 	snippets[snippet.name] = snippet

	base_context = {
					'page': page,
					# 'snippets': snippets,
					'content': content,
					'faculty': faculty,
					'classes': classes,
					'programs': programs,
					'alumni': alumni,
					'article': article,
				
					}
	page_context = get_template_context(page.template_path, request) #get_template_context() is set to default
	final_context = dict(base_context.items() + page_context.items())

	return render_to_response(page.template_path, final_context, context_instance=RequestContext(request), content_type='html')
	