from faculty.models import Faculty
from universal.models import HeaderLogo, Footer

class PageContextLoaderBase(object):
	_request = None

	def __init__(self, request):
		self._request = request

	def get_image_set(self, name):
		try:
			return ImageSet.objects.get(name=name)
		except ImageSet.DoesNotExist:
			return ImageSet()

	def get_context(self):
		return {}


class DefaultPageContextLoader(PageContextLoaderBase):
	def get_context(self):
		header_logo = HeaderLogo.objects.all()
		footer_text = Footer.objects.all()

		return { "header_logo" : header_logo, "footer_text" : footer_text }
	pass

class HomePageContextLoader(PageContextLoaderBase):
	def get_context(self):
		return {}

class FacultyPageContextLoader(PageContextLoaderBase):
	
	def get_context(self):
		faculty = Faculty.objects.all()

		return { "faculty" : faculty }
		

