from django.db import models
from ckeditor.fields import RichTextField

from django.contrib.sites.models import Site



class Page(models.Model):
	
	TEMPLATE_CHOICES = (
		('pages/contact/contact.html', 'Contact'),
		
		('pages/training/programs.html', 'Training Programs'),
		('pages/training/classes.html', 'Training Classes'),
		('pages/training/calendar.html', 'Training Calendar'),
		('pages/training/admission.html', 'Training Admission'),
	
		
		('pages/studio/philosophy.html', 'Studio Philosophy'),
		('pages/studio/faculty.html', 'Studio Faculty'),
		('pages/studio/articles.html', 'Studio Articles'),
		('pages/studio/location.html', 'Studio Location'),
		('pages/studio/testimonials.html', 'Studio Testimonials'),
		('pages/studio/a_meisner.html', 'Studio Meisner'),
		('pages/studio/a_chekhov.html', 'Studio Chekhov'),

	)

	url = models.CharField(max_length=255,
						   default='',
						   null=False,
						   blank=True)

	title = models.CharField(max_length=120,
							 default='',
							 null=False,
							 blank=False)

	template_path = models.CharField(max_length=64,
									 default='pages/generic.html',
									 verbose_name='template name',
									 choices=TEMPLATE_CHOICES)

	content = RichTextField()

	def __unicode__(self):
		return self.title

	def _get_layout_file_extention(self):
		try:
			return self.layout_name.split('.')[-1]
		except IndexError:
			return None

	def get_content_type(self):
		content_types = {}
		content_types['html'] = 'text/html'
		content_types['xml'] = 'text/xml'

		ext = self._get_layout_file_extention()

		return content_types.get(ext, 'html')

	class Meta:
		ordering = ['title']

	def get_absolute_url(self):
		return  '/'+self.url




