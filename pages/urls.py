from django.conf.urls import patterns, url
from views import detail

from articles.views import display_article

urlpatterns = patterns('',
	# url(r'^$', index),
	url(r'^studio/library/(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_article),
	#url(r'^(?P<page_url>[A-Za-z0-9_\-\.]+)/$', detail),
)
