# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(default=b'', max_length=255, blank=True)),
                ('title', models.CharField(default=b'', max_length=120)),
                ('template_path', models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'pages/generic.html', b'Generic'), (b'pages/generic.xml', b'Generic XML'), (b'pages/contact.html', b'Contact'), (b'pages/jobs.html', b'Jobs'), (b'pages/studio.html', b'Studio'), (b'pages/home.html', b'Home'), (b'pages/timeline.html', b'Timeline'), (b'pages/games.html', b'Games'), (b'pages/community.html', b'Community'), (b'pages/press.html', b'Press'), (b'pages/press_archive.html', b'Press Archive'), (b'pages/blog.html', b'Blog'), (b'pages/harmonix-newsletter.html', b'Harmonix Newsletter'), (b'pages/livestream.html', b'Livestream'), (b'pages/podcast.html', b'Podcast'), (b'pages/rss.xml', b'RSS'), (b'pages/management.html', b'Management'), (b'pages/music.html', b'Music'), (b'pages/romance.html', b'Romance Central'), (b'pages/past-projects.html', b'Past Projects')])),
            ],
            options={
                'ordering': ['title'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Snippet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=255)),
                ('content', models.TextField(default=b'', help_text=b'Can be plain text, markdown, or HTML.')),
                ('page', models.ForeignKey(blank=True, to='pages.Page', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
