# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0006_auto_20150717_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snippet',
            name='content',
            field=ckeditor.fields.RichTextField(),
            preserve_default=True,
        ),
    ]
