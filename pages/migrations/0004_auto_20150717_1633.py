# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20150717_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template_path',
            field=models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'pages/contact/contact.html', b'Contact'), (b'pages/training/programs.html', b'Training Programs'), (b'pages/training/classes.html', b'Training Classes'), (b'pages/training/calendar.html', b'Training Calendar'), (b'pages/training/apply.html', b'Training Apply'), (b'pages/studio/philosophy.html', b'Studio Philosophy'), (b'pages/studio/faculty.html', b'Studio Faculty'), (b'pages/studio/articles.html', b'Studio Articles'), (b'pages/studio/location.html', b'Studio Location'), (b'pages/studio/testimonial\t.html', b'Studio Testimonial'), (b'pages/studio/a_meisner.html', b'Studio Meisner'), (b'pages/studio/a_chekhov.html', b'Studio Chekhov')]),
            preserve_default=True,
        ),
    ]
