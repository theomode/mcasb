# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0009_auto_20150727_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snippet',
            name='content',
            field=models.TextField(help_text=b'ckeditor', null=True, blank=True),
            preserve_default=True,
        ),
    ]
