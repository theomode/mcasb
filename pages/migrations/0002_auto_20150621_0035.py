# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template_path',
            field=models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'contact/contact.html', b'Contact'), (b'curriculum/curriculum.html', b'Curriculum'), (b'testimonials/testimonials.html', b'Testimonials'), (b'splash/splash.html', b'Splash'), (b'studio/studio.html', b'Studio'), (b'studio/sub/alumni.html', b'Alumni'), (b'studio/sub/manifesto.html', b'Manifesto'), (b'studio/sub/scott.html', b'Scott'), (b'studio/sub/train.html', b'Train'), (b'training/training.html', b'Training'), (b'training/sub/chekhov.html', b'Chekov'), (b'training/sub/meisner.html', b'Meisner')]),
            preserve_default=True,
        ),
    ]
