# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0010_auto_20150727_1341'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='snippet',
            name='page',
        ),
        migrations.DeleteModel(
            name='Snippet',
        ),
    ]
