from django.db import models
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField
from blog.models import Post
# Create your models here.

class BackgroundImage(models.Model):
	title = models.CharField(max_length=255)
	image = S3DirectField(dest='imgs', blank=True, help_text="<p>width:1920px height:800px</p>")

	def __unicode__(self):
		return self.title

class SiteText(models.Model):
	name = models.CharField(max_length=255)
	splash_title = RichTextField()
	top = models.IntegerField(max_length=255, null=True)
	left = models.IntegerField(max_length=255, null=True)
	mobile_top = models.IntegerField(max_length=255, null=True)

	def __unicode__(self):
		return self.name

class FeaturedImage(models.Model):
	featured = models.ForeignKey(BackgroundImage)

	def __unicode__(self):
		return self.featured.title

class WelcomeLetter(models.Model):
	title = models.CharField(max_length=255)
	letter = RichTextField()


	def __unicode__(self):
		return self.title

class Video(models.Model):
	title = models.CharField(max_length=255)
	embed = models.TextField()

	def __unicode__(self):
		return self.title

class FeaturedVideo(models.Model):
	featured = models.ForeignKey(Video)

	def __unicode__(self):
		return self.featured.title

class News(models.Model):
	featured_post = models.ManyToManyField(Post,blank=True, null=True)

class FeaturedPost(models.Model):
	feat_post = models.ForeignKey(Post,blank=True, null=True)

	def __unicode__(self):
		return self.feat_post.title


class Billboard(models.Model):
	name 	= models.CharField(max_length=255,blank=True, null=True)
	image	= S3DirectField(dest='imgs', blank=True)
	text	= RichTextField(blank=True)
	position = models.IntegerField(default=0)
	background_color = models.CharField(max_length=255,blank=True, null=True)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['position']





