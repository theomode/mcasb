from django.contrib import admin
from splash.models import BackgroundImage, FeaturedImage, WelcomeLetter, Video, FeaturedVideo, FeaturedPost, SiteText, Billboard
# Register your models here.


admin.site.register(FeaturedImage)
admin.site.register(BackgroundImage)
admin.site.register(WelcomeLetter)
admin.site.register(Video)
admin.site.register(FeaturedVideo)
admin.site.register(FeaturedPost)
admin.site.register(SiteText)
admin.site.register(Billboard)