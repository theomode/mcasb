from django.conf.urls import patterns, url

from splash import views
from articles.views import display_article

urlpatterns = patterns('',
	# url(r'^studio/articles/(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_article),
	url(r'^$', views.splash),
)
