# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0005_news'),
    ]

    operations = [
        migrations.AddField(
            model_name='welcomeletter',
            name='ckletter',
            field=ckeditor.fields.RichTextField(default=''),
            preserve_default=False,
        ),
    ]
