# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0016_sitetext_mobile_top'),
    ]

    operations = [
        migrations.AddField(
            model_name='billboard',
            name='background_color',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
