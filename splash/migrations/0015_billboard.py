# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0014_sitetext_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Billboard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(blank=True)),
                ('text', ckeditor.fields.RichTextField(blank=True)),
                ('position', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
    ]
