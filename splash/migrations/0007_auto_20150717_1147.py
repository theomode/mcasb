# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0006_welcomeletter_ckletter'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='welcomeletter',
            name='ckletter',
        ),
        migrations.AlterField(
            model_name='welcomeletter',
            name='letter',
            field=ckeditor.fields.RichTextField(),
            preserve_default=True,
        ),
    ]
