# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20150729_0108'),
        ('splash', '0011_auto_20150720_1512'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeaturedPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feat_post', models.ForeignKey(blank=True, to='blog.Post', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
