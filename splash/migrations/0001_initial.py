# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slide_name', models.CharField(default=b'slide', max_length=255)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('text', models.TextField(help_text=b'ckeditor', null=True, blank=True)),
                ('position', models.IntegerField(default=1)),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slideshow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Splash',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('quote', models.TextField(null=True, blank=True)),
                ('show_posts', models.BooleanField(default=False)),
                ('slideshow', models.ForeignKey(default=b'', blank=True, to='splash.Slideshow')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='slide',
            name='slideshow',
            field=models.ForeignKey(to='splash.Slideshow'),
            preserve_default=True,
        ),
    ]
