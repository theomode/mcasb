# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0012_featuredpost'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sitetext',
            name='director',
        ),
        migrations.RemoveField(
            model_name='sitetext',
            name='main_title',
        ),
        migrations.RemoveField(
            model_name='sitetext',
            name='motto',
        ),
        migrations.RemoveField(
            model_name='sitetext',
            name='sub_title',
        ),
        migrations.AddField(
            model_name='sitetext',
            name='splash_title',
            field=ckeditor.fields.RichTextField(default=''),
            preserve_default=False,
        ),
    ]
