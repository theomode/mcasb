# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0007_auto_20150717_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='featuredvideo',
            name='featured',
            field=models.ForeignKey(to='splash.Video'),
            preserve_default=True,
        ),
    ]
