# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0008_auto_20150717_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='embed',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
