# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SplashPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('image', s3direct.fields.S3DirectField(help_text=b'<p>width:1920px height:800px</p>', blank=True)),
                ('featured', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='slide',
            name='slideshow',
        ),
        migrations.DeleteModel(
            name='Slide',
        ),
        migrations.RemoveField(
            model_name='splash',
            name='slideshow',
        ),
        migrations.DeleteModel(
            name='Slideshow',
        ),
        migrations.DeleteModel(
            name='Splash',
        ),
    ]
