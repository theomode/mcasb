# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0009_auto_20150717_1209'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('main_title', models.CharField(max_length=255)),
                ('sub_title', models.CharField(max_length=255)),
                ('motto', models.CharField(max_length=255)),
                ('director', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
