# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0010_sitetext'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitetext',
            name='left',
            field=models.IntegerField(max_length=255, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sitetext',
            name='top',
            field=models.IntegerField(max_length=255, null=True),
            preserve_default=True,
        ),
    ]
