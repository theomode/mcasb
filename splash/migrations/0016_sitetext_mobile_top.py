# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0015_billboard'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitetext',
            name='mobile_top',
            field=models.IntegerField(max_length=255, null=True),
            preserve_default=True,
        ),
    ]
