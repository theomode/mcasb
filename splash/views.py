from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404

from django.template import RequestContext

from django.shortcuts import render_to_response

from splash.models import FeaturedImage, WelcomeLetter, FeaturedVideo, SiteText, FeaturedPost, Billboard
from blog.models import Post
from alumni.models import Alumni

# Create your views here.

def splash(request):
	feat = FeaturedImage.objects.all()[0]
	welcome = WelcomeLetter.objects.all()[0]
	video = FeaturedVideo.objects.all()[0]
	site = SiteText.objects.all()[0]
	feat_post = FeaturedPost.objects.all()[0]
	alumni = Alumni.objects.all()
	billboard = Billboard.objects.all()

	context = { "feat" : feat, "welcome": welcome, "video": video, "site": site, "feat_post": feat_post, "alumni": alumni, "billboard": billboard }
	return render(request, 'splash/splash.html', context)



