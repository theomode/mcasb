from django.db import models
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField


# Create your models here.
class Footer(models.Model):
	title  = models.CharField(max_length=255, default='')
	mission = models.TextField(default='')

	def __unicode__(self):
		return self.title

class HeaderLogo(models.Model):
	title   = models.CharField(max_length=255, default='')
	logo 	= S3DirectField(dest='imgs', blank=True)
	
	def __unicode__(self):
		return self.title
