from django.contrib import admin
from universal.models import HeaderLogo, Footer


admin.site.register(Footer)
admin.site.register(HeaderLogo)

# Register your models here.
