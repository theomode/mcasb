from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from universal.models import HeaderLogo, Footer

# Create your views here.
def universal(request):
	header_logo = HeaderLogo.objects.all()[0]
	footer_text = Footer.objects.all()[0]

	context = { "header_logo" : header_logo, "footer_text": footer_text }

	return render(request, 'base.html', context)
