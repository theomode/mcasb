# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faculty', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='faculty',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='faculty',
            name='position',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
