from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from s3direct.fields import S3DirectField
from ckeditor.fields import RichTextField

# Create your models here.

class Faculty(models.Model):
	image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	name 	= models.CharField(max_length=255,blank=True, null=True)
	title 	= models.CharField(max_length=255,blank=True, null=True)
	link 	= models.URLField(blank=True, null=True)	
	body	= RichTextField()
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['position']
