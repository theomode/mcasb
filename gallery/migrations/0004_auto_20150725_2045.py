# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0003_auto_20150716_2306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='embed',
            field=models.TextField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
