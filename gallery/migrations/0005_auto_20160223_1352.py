# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-23 13:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0004_auto_20150725_2045'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='slidephoto',
            options={'ordering': ['position']},
        ),
        migrations.AlterModelOptions(
            name='video',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='slidephoto',
            name='position',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='video',
            name='position',
            field=models.IntegerField(default=0),
        ),
    ]
