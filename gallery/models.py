from django.db import models
from s3direct.fields import S3DirectField


# Create your models here.
class Video(models.Model):
	title = models.CharField(max_length=255, blank=True, null=True)
	embed = models.TextField(max_length=255, blank=True, null=True)
	position 	= models.IntegerField(default=0)


	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['position']

class SlidePhoto(models.Model):
	title = models.CharField(max_length=255, blank=True, null=True)
	upload	= S3DirectField(dest='imgs', blank=True, help_text="<p>at least width:1280px Height:800px</p>")
	position 	= models.IntegerField(default=0)


	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['position']

class Friend(models.Model):
	name = models.CharField(max_length=255, blank=True, null=True)
	link = models.URLField()

	def __unicode__(self):
		return self.name