from django.contrib import admin

# Register your models here.
from gallery.models import Video, SlidePhoto, Friend

class VideoAdmin(admin.ModelAdmin):
	ordering = ('position',)
	list_display = ('title', 'position')

class SlidePhotoAdmin(admin.ModelAdmin):
	ordering = ('position',)
	list_display = ('title', 'position')


admin.site.register(Video,VideoAdmin)
admin.site.register(SlidePhoto,SlidePhotoAdmin)
admin.site.register(Friend) 