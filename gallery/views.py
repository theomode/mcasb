from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response
from gallery.models import Video, SlidePhoto, Friend



# Create your views here.
def gallery(request):
	photos = SlidePhoto.objects.all()
	videos = Video.objects.all()
	friend = Friend.objects.all()


	context = { "photos" : photos, "videos" : videos, "friend" : friend }
	return render(request, 'gallery/gallery.html', context)